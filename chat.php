<?php
session_start();
require ('./script/functions.php');
$bdd = bdd_connect();

if ($_SESSION['pseudo'] == NULL) {
    header('Location: index.php');
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Chat : <?php echo $_SESSION['pseudo']; ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" href="./css/bootstrap-theme.css" />
        <link rel="stylesheet" type="text/css" href="./css/style.css" />
        <script href="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/script.js"></script>
		<script href="js/getMessage.js" type="text/javascript"></script>
    </head>
    
    <body onLoad="request(readData), request_status(readData_status)">
 
    <noscript>
    <meta http-equiv="refresh" content="0;URL=./script/no-js.htm">
    </noscript>
 
 
    <span id="hello">
    <?php
    hello(); 
    ?>
    </span>
 
	

    <form action="#" method="post">
        <label for="message"></label><textarea onKeyPress="if(event.keyCode==13){post(); clear();}" name="message" id="message"  rows="5" cols="25" placeholder="Message ..."></textarea>
        <input type="button" onClick="post(), clear()" value="Envoyer !" />
    </form>
    
    <div id="change_status">
		<form action="#" method="post">
		<select name="status" id="status">
		<option value="#" selected>------</option>
		<option value="0">En ligne</option>
		<option value="1">Occupé</option>
		<option value="2">Absent</option>
		</select>
		<input type="button" value="Ok" onClick="set_status()" />
    </form>
    </div>
    <a id="agest" href="./gestion/">Gestion du compte</a>
	<span id="deconnexion"><a href="./script/deconnexion.php">Déconnexion</a></span>

    <div id="ad">Connectés :
    <div id="membres_connectes">
    </div>
    <div>

	<div id="cadre_chat">

	</div>
	
    </body>
</html>
